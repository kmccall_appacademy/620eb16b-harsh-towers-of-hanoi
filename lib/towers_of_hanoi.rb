# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi

  attr_accessor :towers

  def initialize
    @towers = [[3,2,1],[],[]]
  end

  def play
    puts "Welcome to Towers of Hanoi"
    turn = 0
    render
    until won?

      get_move(turn)
      turn+=1
      #clearing screen, only works on UNIX
      puts "\e[H\e[2J"
      render
    end

    puts "You Won in #{turn} turns !"

  end

  def get_move(turn)
    puts "Turn #{turn} : "
    puts "Select Tower to move from : "
    from_tower = gets.to_i
    puts "Select Tower to move from : "
    to_tower = gets.to_i
    move(from_tower,to_tower) if valid_move?(from_tower,to_tower)
  end

  def render
    puts "\n\n"
    puts "#{render_block(@towers[0][2])} #{render_block(@towers[1][2])} #{render_block(@towers[2][2])}"
    puts "#{render_block(@towers[0][1])}#{render_block(@towers[1][1])}#{render_block(@towers[2][1])}"
    puts "#{render_block(@towers[0][0])}#{render_block(@towers[1][0])}#{render_block(@towers[2][0])}"
    puts "\n\n"
  end

  def render_block(int)
    case int
    when nil
      return "         "
    when 1
      return "   @@@   "
    when 2
      return "  @@@@@  "
    when 3
      return " @@@@@@@ "
    end
  end

  def won?
    @towers[1]==[3,2,1] || @towers[2]==[3,2,1]
  end

  def valid_move?(from_tower,to_tower)
    from = @towers[from_tower]
    to = @towers[to_tower]

    if from.empty?
      puts "Invalid Move"
      return false

    elsif !to.empty? && to.last < from.last
      puts "Invalid Move"
      return false

    else
      puts "Moving from Tower #{from_tower} to Tower #{to_tower}"
      return true
    end
  end

  def move(from_tower,to_tower)
    @towers[to_tower].push(@towers[from_tower].pop)
  end

end
